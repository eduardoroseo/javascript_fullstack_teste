import Vue from 'vue'
import VueSweetalert2 from 'vue-sweetalert2';
import App from './App.vue'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
 
Vue.use(VueSweetalert2)
// Vue.use($)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
