import { http } from './config'

export default {

    listar: (query = '', page = 1) => {
        return http.get(`chamado?page=${page}&query=${query}`)
    },

    salvar: (chamado) => {
        return http.post('chamado', chamado)
    },

    atualizar: (id, chamado) => {
        return http.put(`chamado/${id}`, chamado)
    }

}