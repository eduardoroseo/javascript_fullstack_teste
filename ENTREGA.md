# TESTE PHP MOBILE SAÚDE - Eduardo Roseo

Frontend para o teste: https://bitbucket.org/mobilesaude2/teste-php/src/master/

# INSTRUÇÕES PARA EXECUÇÃO

- Executar o projeto no backend primeiramente que pode ser encontrado no endereço: https://bitbucket.org/eduardoroseo/teste-php/src/master/ juntamente com instruções para execução no arquivo Entrega.md

- Como rodar o projeto:
    - Executar o comando: 
    - <code>npm install </code>
    - <code>npm run serve </code>
    - Abrir navegador no endereço: <code>http://localhost:8080/</code>
    
# SOBRE O BACKEND:

O backend encontra-se no link: https://bitbucket.org/mobilesaude2/teste-php/src/master/
Foi feito um fork e um Pull Request para avaliação. 
Já contém mais informações devido ser a entrega principal

# CONTATO:

Fico a inteira disposição pelos contatos
(85) 98817-9596 telefone e whatsapp
eduardoroseo@gmail.com